package tencentcloud

import (
	goerr "errors"
	"fmt"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/errors"
	vpc "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/vpc/v20170312"
)

type VpcNetwork interface {
}

func (r *RealTencentCloud) DefaultSecurityGroup() (string, error) {
	request := vpc.NewDescribeSecurityGroupsRequest()
	request.Filters = []*vpc.Filter{
		&vpc.Filter{
			Name:   common.StringPtr("security-group-name"),
			Values: common.StringPtrs([]string{"default"}),
		},
	}
	response, err := r.vpcClient.DescribeSecurityGroups(request)
	if _, ok := err.(*errors.TencentCloudSDKError); ok || err != nil {
		fmt.Println("获取安全组失败", err)
		return "", err
	}
	if *response.Response.TotalCount < 1 {
		return "", goerr.New("安全组为空")
	}

	return *response.Response.SecurityGroupSet[0].SecurityGroupId, nil
}
