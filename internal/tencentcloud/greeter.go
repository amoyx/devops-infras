package tencentcloud

import (
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"
	redis "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/redis/v20180412"
	tse "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/tse/v20201207"
	vpc "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/vpc/v20170312"
)

const (
	DefaultRegion string = "ap-shanghai"
	TseEndpoint   string = "tse.tencentcloudapi.com"
	RedisEndpoint string = "redis.tencentcloudapi.com"
	VpcEndpoint   string = "vpc.tencentcloudapi.com"
)

type TencentCloud interface {
	NacosCloud
}

type RealTencentCloud struct {
	tseClient   *tse.Client
	redisClient *redis.Client
	vpcClient   *vpc.Client
}

func NewRealTencentCloud(secretId, secretKey string) *RealTencentCloud {
	credential := common.NewCredential(
		secretId,
		secretKey,
	)
	tsecpf := profile.NewClientProfile()
	tsecpf.HttpProfile.Endpoint = TseEndpoint
	rediscpf := profile.NewClientProfile()
	rediscpf.HttpProfile.Endpoint = RedisEndpoint
	vpccpf := profile.NewClientProfile()
	vpccpf.HttpProfile.Endpoint = VpcEndpoint

	redisClient, _ := redis.NewClient(credential, DefaultRegion, rediscpf)
	tseClient, _ := tse.NewClient(credential, DefaultRegion, tsecpf)
	vpcClient, _ := vpc.NewClient(credential, DefaultRegion, vpccpf)

	return &RealTencentCloud{
		tseClient:   tseClient,
		redisClient: redisClient,
		vpcClient:   vpcClient,
	}
}
