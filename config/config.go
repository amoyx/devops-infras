package config

type Bootstrap struct {
	Application *Application `protobuf:"bytes,1,opt,name=application,proto3" json:"application,omitempty"`
	Nacos       *Nacos       `protobuf:"bytes,3,opt,name=nacos,proto3" json:"nacos,omitempty"`
}

type Application struct {
	Server       *Server       `protobuf:"bytes,1,opt,name=server,proto3" json:"server,omitempty"`
	Data         *Data         `protobuf:"bytes,2,opt,name=data,proto3" json:"data,omitempty"`
	TencentCloud *TencentCloud `protobuf:"bytes,4,opt,name=tencentCloud,proto3" json:"tencentCloud,omitempty"`
}

type Data struct {
	Database *Data_Database `protobuf:"bytes,1,opt,name=database,proto3" json:"database,omitempty"`
	Redis    *Data_Redis    `protobuf:"bytes,2,opt,name=redis,proto3" json:"redis,omitempty"`
}

type Data_Database struct {
	Driver string `protobuf:"bytes,1,opt,name=driver,proto3" json:"driver,omitempty"`
	Source string `protobuf:"bytes,2,opt,name=source,proto3" json:"source,omitempty"`
}

type Data_Redis struct {
	Addr         string `protobuf:"bytes,1,opt,name=addr,proto3" json:"addr,omitempty"`
	Password     string `protobuf:"bytes,2,opt,name=password,proto3" json:"password,omitempty"`
	Type         string `protobuf:"bytes,3,opt,name=type,proto3" json:"type,omitempty"`
	Index        int32  `protobuf:"varint,4,opt,name=index,proto3" json:"index,omitempty"`
	ReadTimeout  string `protobuf:"bytes,5,opt,name=read_timeout,json=readTimeout,proto3" json:"read_timeout,omitempty"`
	WriteTimeout string `protobuf:"bytes,6,opt,name=write_timeout,json=writeTimeout,proto3" json:"write_timeout,omitempty"`
}

type Nacos struct {
	Ip        string `protobuf:"bytes,1,opt,name=ip,proto3" json:"ip,omitempty"`
	Port      uint64 `protobuf:"varint,2,opt,name=port,proto3" json:"port,omitempty"`
	Group     string `protobuf:"bytes,3,opt,name=group,proto3" json:"group,omitempty"`
	DataId    string `protobuf:"bytes,4,opt,name=dataId,proto3" json:"dataId,omitempty"`
	Namespace string `protobuf:"bytes,5,opt,name=namespace,proto3" json:"namespace,omitempty"`
}

type TencentCloud struct {
	SecretId    string `protobuf:"bytes,1,opt,name=secretId,proto3" json:"secretId,omitempty"`
	SecretKey   string `protobuf:"bytes,1,opt,name=secretKey,proto3" json:"secretKey,omitempty"`
	ClusterName string `protobuf:"bytes,1,opt,name=clusterName,proto3" json:"clusterName,omitempty"`
	Namespaces  string `protobuf:"bytes,1,opt,name=namespaces,proto3" json:"namespaces,omitempty"`
}

type Server struct {
	Http *Server_HTTP `protobuf:"bytes,1,opt,name=http,proto3" json:"http,omitempty"`
	Grpc *Server_GRPC `protobuf:"bytes,2,opt,name=grpc,proto3" json:"grpc,omitempty"`
}

type Server_HTTP struct {
	Network string `protobuf:"bytes,1,opt,name=network,proto3" json:"network,omitempty"`
	Addr    string `protobuf:"bytes,2,opt,name=addr,proto3" json:"addr,omitempty"`
	Timeout string `protobuf:"bytes,3,opt,name=timeout,proto3" json:"timeout,omitempty"`
}

type Server_GRPC struct {
	Network string `protobuf:"bytes,1,opt,name=network,proto3" json:"network,omitempty"`
	Addr    string `protobuf:"bytes,2,opt,name=addr,proto3" json:"addr,omitempty"`
	Timeout string `protobuf:"bytes,3,opt,name=timeout,proto3" json:"timeout,omitempty"`
}
