package v1

type StatusConst string

const (
	DefaultStatusConst StatusConst = "creating"
	//ProcessingStatusConst StatusConst = "processing"
	FailStatus     StatusConst = "fail"
	SuccessStatus  StatusConst = "success"
	CreatingStatus StatusConst = "creating"
)
