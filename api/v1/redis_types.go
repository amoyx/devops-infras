/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// RedisSpec defines the desired state of Redis
type RedisSpec struct {
	Region       string `json:"region,omitempty"`       // 城市
	VpcId        string `json:"vpcId,omitempty"`        // vpc id
	SubnetId     string `json:"subnetId,omitempty"`     //  subnet id
	ZoneId       uint64 `json:"zoneId,omitempty"`       // 可用区
	TypeId       uint64 `json:"typeId,omitempty"`       // 实例类型
	InstanceName string `json:"instanceName,omitempty"` // 实例名称
	MemSize      uint64 `json:"memSize,omitempty"`      // 内存大小，单位MB
	GoodsNum     uint64 `json:"goodsNum,omitempty"`     // 实例数量
	Password     string `json:"password,omitempty"`     // redis密码
	BillingMode  int64  `json:"billingMode,omitempty"`  // 计费模式， 0按量计费，1包年包月
	Period       uint64 `json:"period,omitempty"`       // 购买时长，1,2,3,4,5,6,7,8,9,10,11,12,24,36月
	VPort        uint64 `json:"VPort,omitempty"`        // 端口，默认6379
}

// RedisStatus defines the observed state of Redis
type RedisStatus struct {
	Status    StatusConst     `json:"status"`
	GoodsNum  uint64          `json:"goodsNum"`
	Message   string          `json:"message"`
	Instances []RedisInstance `json:"instances"`
}

type RedisInstance struct {
	Id             string      `json:"id"`
	Status         StatusConst `json:"status"`
	PrivateAddress string      `json:"privateAddress"` // 私网地址
	WanAddress     string      `json:"wanAddress"`     // 公网地址
	Message        string      `json:"message"`        // 信息
	ExternalStatus StatusConst `json:"externalStatus"` // 公网状态
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Redis is the Schema for the redis API
type Redis struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RedisSpec   `json:"spec,omitempty"`
	Status RedisStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// RedisList contains a list of Redis
type RedisList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Redis `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Redis{}, &RedisList{})
}
