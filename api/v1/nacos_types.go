/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// NacosSpec defines the desired state of Nacos
type NacosSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Name               string `json:"name"`
	VpcId              string `json:"vpcId"`
	Subnet             string `json:"subnet"`
	EngineVersion      string `json:"engineVersion"`
	EngineResourceSpec string `json:"engineResourceSpec"`
}

// NacosStatus defines the observed state of Nacos
type NacosStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Id              string      `json:"id"`
	Status          StatusConst `json:"status"`
	IntranetAddress string      `json:"intranetAddress"` // 内网地址
	InternetAddress string      `json:"internetAddress"` // 公网地址
	ExternalStatus  StatusConst `json:"externalStatus"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Nacos is the Schema for the nacos API
type Nacos struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   NacosSpec   `json:"spec,omitempty"`
	Status NacosStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// NacosList contains a list of Nacos
type NacosList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Nacos `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Nacos{}, &NacosList{})
}
